
const _firebase = require("firebase");
require("firebase/firestore");
// Initializing FirebaseAPp
_firebase.initializeApp({
    apiKey: 'AIzaSyC02R_TIvNdhgdtm91kAAPGTkeom9Qgua0',
    authDomain: 'news-604ef.firebaseapp.com',
    projectId: 'news-604ef',
    databaseURL: "https://news-604ef.firebaseio.com",
    storageBucket: "news-604ef.appspot.com",
    messagingSenderId: "460665824027"
});

// Initialize Cloud Firestore through Firebase
var db = _firebase.firestore();
const settings = {/* your settings... */ timestampsInSnapshots: true};
db.settings(settings);

export const newsDB = db.collection("news");
export const userDB = db.collection("user");
export const firebase = _firebase;
export default db;
