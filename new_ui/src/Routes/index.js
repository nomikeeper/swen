export default {
    Home: "/",
    Library: "/Library",
    CreateNews: "/CreateNews",
    News: '/News',
    Draft: '/Draft'
}