import Web3 from 'web3';

const web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:7545"));

let NewsAbi =  [
  {
    "constant": true,
    "inputs": [
      {
        "name": "",
        "type": "uint256"
      }
    ],
    "name": "NewsArchive",
    "outputs": [
      {
        "name": "title",
        "type": "string"
      },
      {
        "name": "context",
        "type": "string"
      },
      {
        "name": "date",
        "type": "string"
      }
    ],
    "payable": false,
    "stateMutability": "view",
    "type": "function"
  },
  {
    "constant": true,
    "inputs": [
      {
        "name": "",
        "type": "uint256"
      }
    ],
    "name": "newsAddresses",
    "outputs": [
      {
        "name": "",
        "type": "address"
      }
    ],
    "payable": false,
    "stateMutability": "view",
    "type": "function"
  },
  {
    "inputs": [],
    "payable": false,
    "stateMutability": "nonpayable",
    "type": "constructor"
  },
  {
    "constant": false,
    "inputs": [
      {
        "name": "_title",
        "type": "string"
      },
      {
        "name": "_context",
        "type": "string"
      },
      {
        "name": "_date",
        "type": "string"
      }
    ],
    "name": "createNews",
    "outputs": [
      {
        "name": "",
        "type": "uint256"
      }
    ],
    "payable": false,
    "stateMutability": "nonpayable",
    "type": "function"
  },
  {
    "constant": true,
    "inputs": [
      {
        "name": "_index",
        "type": "uint256"
      }
    ],
    "name": "getNews",
    "outputs": [
      {
        "name": "_title",
        "type": "string"
      },
      {
        "name": "_context",
        "type": "string"
      },
      {
        "name": "_date",
        "type": "string"
      }
    ],
    "payable": false,
    "stateMutability": "view",
    "type": "function"
  },
  {
    "constant": true,
    "inputs": [],
    "name": "getAllNews",
    "outputs": [
      {
        "name": "",
        "type": "address[]"
      }
    ],
    "payable": false,
    "stateMutability": "view",
    "type": "function"
  },
  {
    "constant": true,
    "inputs": [],
    "name": "totalNewsCount",
    "outputs": [
      {
        "name": "",
        "type": "uint256"
      }
    ],
    "payable": false,
    "stateMutability": "view",
    "type": "function"
  }
];
let NewsAddress = '0x91E02348ae38FB59950a4f6Eaf007D8Ea8806BC8';
web3.eth.defaultAccount = web3.eth.accounts[0]

console.log("DEtail: ", web3.eth);
const newsContract = new web3.eth.Contract(NewsAbi, NewsAddress, {
  from: '0x1234567890123456789012345678901234567891', // default from address
  gasPrice: '20000000000' // default gas price in wei, 20 gwei in this case
});
console.log('News contract: ', newsContract.methods.totalNewsCount().call({from: '0xde0B295669a9FD93d5F28D9Ec85E40f4cb697BAe'}, function(error, result){
  if(error)
    console.log("Error: ", error);
  else
    console.log("Result: ", result);
}));
export { newsContract };