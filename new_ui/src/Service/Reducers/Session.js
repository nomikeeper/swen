import Actions from "../Actions/Session";

const InitialState = {
    user: '',
}

const SessionReducer = (state = InitialState, action) => {
    switch(action.type){
        case Actions.SetUser:{
            return Object.assign({}, state,{
                user: action.user
            });
        }
        case Actions.ClearUser:{
            return Object.assign({}, state, {
                user: null
            });
        }
        default:{
            return state;
        }
    }
}

export default SessionReducer;