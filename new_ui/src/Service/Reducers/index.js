import { combineReducers } from "redux";
import { sessionReducer } from "redux-react-session";

// ------ Importing Users
import Session from "./Session";

export default combineReducers({
    Session: sessionReducer,
})