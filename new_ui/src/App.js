import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
// ------ Importing Components
import { Provider, connect} from "react-redux";
// ------ Importing store
import store from "./Store";
import { sessionService } from "redux-react-session";
import { newsContract } from "./setup";
import { BrowserRouter, Route } from "react-router-dom";
import Routes from "./Routes";
// Views
import Home from "./Views/Home";
import Home2 from "./Views/Home/index2"
import Contact from "./Views/Library";
import CreateNews from "./Views/CreateNews";
import News from "./Views/News";
import CKEditor from "./Views/Draft";
import Router from 'react-router-dom/Router';

class App extends Component {

  _doSomething(){
  }

  render() {
    return (
      <BrowserRouter>
        <div id="app-wrapper">
          <Route path={'/main'} component={Home} exact />
          <Route path={Routes.Home} component={Home2} exact />
          <Route path={Routes.Library} component={Contact} />
          <Route path={Routes.CreateNews} component={CreateNews} />
          <Route path={`${Routes.News}/:id`} component={News} />
          <Route path={Routes.Draft} component={CKEditor} />
        </div>
      </BrowserRouter>
    );
  }
}

export default () => {

  const validateSession = (session) => {
    // check if your session is still valid
    return true;
  }

  const options = { refreshOnCheckAuth: true, redirectPath: '/main', driver: 'COOKIES', validateSession };
  
  sessionService.initSessionService(store,options)
    .then(() => console.log('Redux React Session is ready and a session was refreshed from your storage'))
    .catch(() => console.log('Redux React Session is ready and there is no session in your storage'));
  return(
    <Provider store={store}>
      <App />
    </Provider>
  );
};
