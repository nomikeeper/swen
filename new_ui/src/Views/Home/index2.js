import React, { Component } from "react";
import Modal from 'react-modal';
// ------ Importing Views
import CreateNews from "../CreateNews";
import Library from "../Library";
import News from "../News"
import SignUp from "../SignUp";
import Dashboard from "../Dashboard";
import Account from "../Account";
import Intro from "../Intro";
import Menu, { MenuOptions } from "../Components/Menu";
import { sessionService } from "redux-react-session";
// ------ Importing Redux
import { connect } from "react-redux";
// ------ Importing styles
require("../../style/css/createNews.css");
require("../../style/css/home.css");

class Index extends Component {
    constructor(props){
        super(props);
        this.state={
            currentView: 'Intro',
            newsID: '',
            isFetched: false,
            user: '',
            modalVisibility: false,
        }
    }

    componentWillReceiveProps(nextProps){
    }

    _changeSelectedView(choice){
        this.setState({ currentView: choice})
    }
    
    _clearSelectedView(){
        this.setState({ currentView: 'Intro' })
    }

    _openModal(){
        if(this.state.modalVisibility == false )
            this.setState({
                modalVisibility: true,
            })
    }

    _closeModal(){
        if(this.state.modalVisibility == true)
            this.setState({
                modalVisibility: false,
                newsID: ''
            })
    }

    _setNewsID(id){
        this.setState({ newsID: id})
        this._openModal();
    }

    _clearNewsID(){
        this.setState({ newsID: ''})
    }

    _selectView(){
        switch(this.state.currentView){
            case MenuOptions.CreateTemplate:{
                // Return Component
                return <CreateNews />
            }
            case MenuOptions.TemplatesArchive:{
                // Return Template Archive
                return <Library 
                            type={MenuOptions.TemplatesArchive} 
                            setNewsID={(id)=> { this._setNewsID(id)}}    
                        />
            }
            case MenuOptions.TemplatesDrafts:{
                // Return Template Drafts
                return <Library 
                            type={MenuOptions.TemplatesDrafts} 
                            setNewsID={(id)=> { this._setNewsID(id)}}    
                        />
            }
            case MenuOptions.NewsDraft:{
                // Return News Draft
                return <Library 
                            type={MenuOptions.NewsDraft} 
                            setNewsID={(id)=> { this._setNewsID(id)}}    
                        />
            }
            case MenuOptions.Dashboard:{
                // Return Dashboard
                return <Dashboard />
            }
            case MenuOptions.Account:{
                // Return Account
                return  <Account 
                            user={this.props.user}
                        />
            }
            case MenuOptions.SignUp:{
                // Return Sign Up 
                return <SignUp />
            }
            case MenuOptions.Intro:{
                return <Intro />
            }
            case MenuOptions.Logout:{
                // Logout the user
                break;
            }
            default:{
                return <Intro />
            }
        }
    }

    render(){
        return(
            <div id="wrapper-2">
                <Menu 
                    currentView={ this.state.currentView }
                    changeSelectedView={(value) => { this._changeSelectedView(value)} }
                    clearSelectedView={ () => { this._clearSelectedView() }}
                    user={this.props.Session.user}
                    dispatch={this.props.dispatch}
                    />
                    <div id="content-2">
                        {
                            this._selectView()
                        }
                    </div>
                <Modal
                    isOpen={this.state.modalVisibility}
                    onRequestClose={() => { this.closeModal }}
                    contentLabel="Example Modal"
                    >
                    <a  className="modal-close"
                        onClick={()=>{ this._closeModal() }}>
                        <ion-icon name="ios-close"></ion-icon>
                    </a>
                    <News />
                </Modal>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    Session: state.Session
})

export default connect(mapStateToProps)(Index);