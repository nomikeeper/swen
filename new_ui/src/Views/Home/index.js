import React, { Component } from "react";
require('../../style/css/home.css');

class Index extends Component{
    constructor(props){
        super(props);
        this.state={

        }
    }

    render(){
        return(
            <div id="wrapper">
                <div id="header">
                    <h3 className="title">News</h3>
                </div>
                <div id="content">
                    <div className="section">
                        <a href="CreateNews">Create News</a>
                    </div>
                    <div className="section">
                        <a href="Library">Library</a>
                    </div>        
                </div>
                <div id="footer">
                    <a>Footer</a>
                </div>
            </div>
        );
    }
};

export default Index;