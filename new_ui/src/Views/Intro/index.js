import React, { Component } from "react";
require("../../style/css/intro.css");

class Index extends Component {
    constructor(props){
        super(props);
        this.state={

        }
    }

    render(){
        return(
            <div id="intro-wrapper">
                <div id="intro">
                    <h1>Welcome to <span>Swen</span></h1>
                    <h3>Write once publish everywhere.</h3>
                </div>
            </div>
        );
    }
}

export default Index;