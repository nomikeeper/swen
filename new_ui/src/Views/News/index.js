import React, { Component } from 'react';
import { Line } from "rc-progress";
import Menu, { MenuOptions } from "../Components/Menu"
import { connect } from "react-redux";
import types from "./types";
import "pretty-checkbox/src/pretty-checkbox.scss";
import 'pretty-checkbox/src/scss/_variables.scss';
import 'pretty-checkbox/src/scss/_core.scss';
import 'pretty-checkbox/src/scss/elements/font-icon/_general.scss';
import 'pretty-checkbox/src/scss/extras/_animation.scss';

require("../../style/css/news.css");

const input_types = {
    title: 'title',
    context: 'context',
    category: 'category',
    tags: 'tags',
}

class Index extends Component{
    constructor(props){
        super(props);
        this.state={
            id: null,
            accuracyRate: 25,
            type: types.Template,
            isEditing: false,
            news: TempValues
        }
    }
    componentWillMount(){
        // Select news id from the url
        // const { id } = this.props.match.params
        // this.setState({ id: id})
        // fetch the news info from the server
    }

    _updateValue(type,value){
        switch(type){
            case input_types.category:{
                this.setState({ news: Object.assign({},this.state.news,{category: value}) });
            }
        }
    }
    _category(){
        if(this.state.isEditing){
            return(
                <div className="category">
                    <label className="label">Category: </label>
                    <select
                        onChange={(e)=>{ 
                            this._updateValue(input_types.category , e.target.value)
                        }}>
                        <option value="" selected disabled hidden>Choose here</option>
                        <option value="News">News</option>
                        <option value="Entertainment">Entertainment</option>
                        <option value="Music">Music</option>
                        <option value="Politics">Politics</option>
                    </select>
                </div>
            );
        } else {
            return(
                <p className="category"><span>Category:</span> Entertainment</p>
            );
        }
    }

    _accuracy(){
        if(this.state.type == types.Template){
            return(
                <div className="accuracy-wrapper">
                    <div className="accuracy-graph">
                        <h3 className="title">Accuracy rate: {this.state.accuracyRate}%</h3>
                        <Line   
                            percent={this.state.accuracyRate} 
                            strokeWidth="1" 
                            strokeColor="#4EF46C"
                            trailColor="#D3D3D3" />
                    </div>
                    <div className="evidence-wrapper">
                        <h3 className="title">Additional Sources</h3>
                        <div className="evidence-list">
                            <div className="evidence">
                                <div className="index">
                                    <h5>1</h5>
                                </div>
                                <div className="content">
                                    <div className="link">
                                        <a href="#">Source title</a>    
                                    </div>
                                    <div className="files">
                                        <a href="#">File</a>
                                    </div>
                                    <div className="date">
                                        <p>2018-06-14</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="add-evidence">
                            <a className="button" onClick={()=>{alert('Add new evidence!')}}>Add Evidence</a>
                        </div>
                    </div>
                </div>
            )
        } else {
            return null
        }
    }
    render(){
        return(
            <div>
                <div id="news">
                    <div className="news-title-wrapper">
                        <h2 className="news-title">News Title</h2>
                        <a className="news-address" onClick={()=>{ alert('View the original news on Blockchain.')}}>{ this.state.id }</a>
                    </div>
                    <div className="paragraph-wrapper">
                        <p className="paragraph">{ this.state.news.context }</p>
                    </div>  
                    <div className="details-wrapper">
                        { this._category() }
                        <p className="tags"><span>Tags:</span> Lorem, Ipsum</p>
                        <p className="date"><span>Date:</span> 2018-06-24</p>
                    </div>
                    {   // Show accuracy information and evidences only if it's legitimate template
                        this._accuracy()
                    }
                    <div className="pretty p-default p-round p-smooth p-plain editCheckBox">
                        <input  type="checkbox" 
                                value={this.state.isEditing}
                                onChange={(e) => { this.setState({ isEditing: e.target.checked })  }}    
                        />
                            <label className={`description ${ this.state.isEditing ? 'editing' : null}`}>Edit</label>
                    </div>

                    <div className="author-wrapper">
                        <div className="author">
                            Author: <a className="author-link" href="#">Nomio</a>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    Session: state.Session
});

export default connect(mapStateToProps)(Index);

const TempValues={
    title: 'First News',
    context: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce nisl quam, elementum non sem ac, pellentesque finibus quam. Nulla aliquam libero risus, ut congue lacus efficitur lacinia. Sed sit amet arcu ac dolor placerat euismod. Etiam placerat orci ut arcu iaculis porta. Nam sed hendrerit dui. Nullam vestibulum justo ut diam maximus, vitae sodales urna volutpat. Quisque vel dictum purus. Cras vitae lacinia urna. Phasellus sollicitudin massa in sem laoreet placerat. Suspendisse potenti. Vivamus a leo non enim pulvinar tempus ut eleifend nunc. Curabitur orci tellus, tempus eu eleifend non, laoreet vitae odio. Cras leo nulla, vulputate at turpis sed, mollis tempor odio. Nullam pretium ultricies felis et malesuada. Sed et mi consectetur, commodo augue nec, tempor nibh.

    Pellentesque orci justo, hendrerit in enim non, dictum aliquet nisl. Donec dignissim sodales ante, id fringilla magna mattis ac. Suspendisse a viverra sem, non tempor turpis. Quisque euismod risus eget tellus porttitor pulvinar ut a nulla. Suspendisse rutrum ut enim at tincidunt. Etiam consequat porttitor urna, id efficitur ante commodo vitae. Etiam sed ipsum ac elit faucibus aliquet. Morbi ut massa in justo rhoncus scelerisque. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam volutpat ultrices feugiat. Sed bibendum, tortor in sollicitudin malesuada, purus dui aliquet elit, eget viverra ante quam ut nisl.

    Donec et mi ligula. Aenean at elementum sapien, at accumsan tellus. Aenean non ligula ut tellus posuere congue. Quisque tincidunt erat arcu, ac condimentum mauris commodo vitae. Sed pharetra sapien velit, et molestie lectus viverra at. Integer vehicula magna metus, et dictum justo accumsan in. Curabitur fermentum sem nisi, quis varius ex tincidunt id.

    In condimentum consectetur ex malesuada varius. Ut feugiat turpis mi, quis scelerisque odio facilisis ut. Cras dui urna, malesuada sed felis eget, semper dictum eros. Nulla eu nunc tortor. Maecenas sit amet urna sit amet enim ullamcorper faucibus viverra sit amet tellus. Integer lorem ipsum, tristique id placerat a, vulputate non nulla. Cras nec tellus nibh.

    Duis ultrices lorem augue, quis tempor sapien vulputate dapibus. Duis dignissim ac velit eget luctus. Etiam ullamcorper risus id cursus congue. Cras tempor nulla et commodo placerat. Quisque mattis fermentum nisi, eu placerat lacus maximus in. Etiam ac pretium orci. Mauris sollicitudin molestie diam non dignissim.
    `,
    category : '',
    tags: ['tag1', 'tag2'],
    author: 'Nomio G'
}