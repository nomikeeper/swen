export default {
    Template: 'Template',
    TemplateDraft: 'TemplateDraft',
    News: 'News',
    NewsDraft: 'NewsDraft'
}