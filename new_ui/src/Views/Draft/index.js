import React, { Component } from 'react';
import {
    Editor,
    createEditorState
} from "medium-draft";
import Headroom from "react-headroom";
import Routes from "../../Routes";

require('../../style/css/draft.css');
require("medium-draft/lib/index.css");

class Index extends Component {
    constructor(props){
        super(props);
        this.state={
            editorState: createEditorState(),
        }

        this.onChange =(editorState) => {
            this.setState({ editorState })
        }

        this.refsEditor = React.createRef();
    }

    componentDidMount(){
        this.refsEditor.current.focus();
    }

    render(){
        const { editorState } = this.state;
        return(
            <div id="draft">
                <Headroom className="head-room">
                    <a className="Logo" href={ Routes.Home }>Swen</a>
                </Headroom>
                <div id="template-selector">
                    <a className="template-selector-btn" onClick={() => { alert('Select template')}}>Select Template</a>
                </div>
                <div id="editor-wrapper">
                    <Editor
                        className="editor"
                        ref={this.refsEditor}
                        editorState={editorState}
                        onChange={this.onChange} />
                </div>
            </div>
        );
    }
}

export default Index;