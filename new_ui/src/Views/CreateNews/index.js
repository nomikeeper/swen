import React, { Component } from "react";
import TagsInput from "react-tagsinput";
import moment from "moment";
import db, { newsDB } from "../../Firebase";
// ------ IMporting smart contract
import { newsContract } from "../../setup";
// Required for side-effects
require("react-tagsinput/react-tagsinput.css");
require("../../style/css/createNews.css");

const types = {
    title: 'title',
    context: 'context',
    category: 'category',
    tags: 'tags',
    terms: 'terms'
}

class Index extends Component {
    constructor(props){
        super(props);
        this.state={
            title: '',
            context: [],
            tags:[],
            category: '',
            terms: false,
            warning: false,
        }
    }

    _updateValue(type, value){
        switch(type){
            case types.title:{
                this.setState({ title: value })
                break;
            }
            case types.context:{
                this.setState({ context: value })
                break;
            }
            case types.category:{
                this.setState({ category: value })
                break;
            }
            case types.terms:{
                this.setState({ terms: value })
            }
            default: {
                return;
            }
        }
    }

    // Validate News
    _validateNews(){
        if( this.state.title 
            && this.state.context 
            && this.state.terms ){
                return true;
        } else {
            return false;
        }
    }
    // crate news
    _createNews(){
        if(this._validateNews()){
            this.setState({
                warning: false,
            })
            console.log("News: ", {
                title: this.state.title,
                context: this.state.context,
                tags: this.state.tags,
                category: this.state.category,
                date: moment().format(),
            })

            console.log("Contract: ", newsContract);
            let id = newsContract.createNews(
                this.state.title,
                this.state.context,
                moment().format(),
            );
            console.log("id", id);
            /*
            newsDB.add({
                title: this.state.title,
                context: this.state.context,
                tags: this.state.tags,
                category: this.state.category,
                date: moment().format(),
            }).then((docRef)=>{
                console.log("Document written with ID: ", docRef.id);
            }).catch((err) => {
                console.error("Error adding document: ", err);
            })
            */
        } else {
            this.setState({
                warning: true,
            })
        }
    }

    // Save as draft
    _saveAsDraft(){
        alert("Save as draft!");
    }

    // Warning message rendering
    _renderWarning(){
        if(this.state.warning){
            return(
                <div className="warning-wrapper">
                    <p className="warning" >News is not valid</p>
                </div>
            )
        }
    }

    // Clears all values
    _clearValues(){
        this.setState({
            title: '',
            context: ''
        })
    }

    // Handle tags change
    _handleTags(tags){
        this.setState({tags})
    }

    // Main render function
    render(){
        return(
            <div id="wrapper">
                <div id="form-wrapper">
                    <form id="form">
                        <h3 className="form-title">Create News</h3>
                        
                        <div className="form-input-wrapper">
                            <label className="label">Title</label>
                            <input 
                                className="input"
                                type="text" 
                                name="title"  
                                value={this.state.title}
                                placeholder={"News title"}
                                onChange={(e)=>{ this._updateValue(types.title, e.target.value) }}
                                />
                        </div>
                        <div className="form-input-wrapper">
                            <label className="label">Context</label>
                            <textarea
                                className="input" 
                                name="context"  
                                value={this.state.context}
                                placeholder={"Context"}
                                onChange={(e)=>{ this._updateValue(types.context, e.target.value) }}
                                />
                        </div>
                        <div className="form-input-wrapper">
                            <label className="label">Files</label>
                            <input type="file" name="files" />
                        </div>
                        <div className="Category-and-tag-wrapper">
                            <label className="label">Category</label>
                            <select
                                onChange={(e)=>{ 
                                    this._updateValue(types.category , e.target.value)
                                }}>
                                <option value="" selected disabled hidden>Choose here</option>
                                <option value="News">News</option>
                                <option value="Entertainment">Entertainment</option>
                                <option value="Music">Music</option>
                                <option value="Politics">Politics</option>
                            </select>
                            <label className="label">Tag</label>
                            <TagsInput
                                value={this.state.tags}
                                onChange={this._handleTags.bind(this)}
                            />
                        </div>
                        <div className="terms-wrapper" >
                            <input 
                                id="terms"
                                type="checkbox"
                                defaultChecked={ false }
                                onChange={(e)=>{this._updateValue(types.terms, e.target.checked )}} 
                            />
                            <label htmlFor="terms">I accept the controller-controller terms. This is required when sharing Analytics data to improve Google Products and Services. Learn more</label>
                        </div>
                        <div className="create-btn-wrapper">
                            <a className="create-btn" onClick={()=>{ this._createNews() }}>Create News</a>
                            
                            <a className="create-btn" onClick={()=>{ this._saveAsDraft() }}>Save as Draft</a>     
                        </div>
                        {
                            this._renderWarning()
                        }
                    </form>
                </div>
            </div>
        );
    }
}

export default Index;