import React, { Component } from "react";
import { connect } from "react-redux";
import { userDB } from "../../Firebase";
import Loader from 'react-loader-spinner';

require("../../style/css/account.css");
require("../../style/css/checkBox.css");

const types = {
    aboutMe: 'aboutMe',
    intro: 'intro',
    eName: 'ename',
    fullName: 'fullName'
}
class Index extends Component {
    constructor(props){
        super(props);
        this.state={
            isFetched: false,
            account: null,
            tempAccount: null,
            isEditing: false,
        }
    }

    _updateField(type, obj, value){
        switch(type){
            case types.aboutMe:{
                obj.aboutMe = value;
                return obj;
            }
            case types.intro:{
                obj.intro = value;
                return obj;
            }
            case types.eName:{
                obj.eName = value;
                return obj;
            }
            case types.fullName:{
                obj.fullName = value;
                return obj;
            }
            default: return obj;
        }
    }

    _updateTempAccount(type, value){
        var _tempAccount;
        if(this.state.tempAccount){
            _tempAccount = Object.assign({}, this.state.tempAccount)
            _tempAccount = this._updateField(type, _tempAccount, value);
        } else {
            _tempAccount = Object.assign({}, this.state.account);
            _tempAccount = this._updateField(type, _tempAccount, value);
        }
        this.setState({ tempAccount: _tempAccount });
    }
    
    componentWillMount(){
        if(!this.state.isFetched){
            userDB.where('uid', '==', this.props.Session.user.uid).get()
                .then(resp => { 
                    this.setState({ 
                        account: resp.docs[0].data(),
                        isFetched: true, 
                    })
                })
                .catch(error => { console.log("error: ", error)})
        }
    }

    _changeEditState(value){
        this.setState({isEditing: value})
    }

    _customAuthor(){
        if(!this.state.isEditing){
            return <p>Author: { this.state.account.fullName}</p>
        } else {
            return <div>Author: 
                            <input  type="text" 
                                placeholder={this.state.account.fullName} 
                                onKeyPress={(e) => { 
                                    console.log('Key Code: ', e.keyCode);
                                    if(e.keyCode == 13)
                                        alert("Enter has been pressed!")
                            }} />
                    </div>
        }
    }

    _customEname(){
        if(!this.state.isEditing){
            return <p>E-name: { this.state.account.eName}</p>
        } else {
            return <div>Author: 
                            <input  type="text" 
                                placeholder={this.state.account.eName} 
                                onKeyPress={(e) => { 
                                    console.log('Key Code: ', e.keyCode);
                                    if(e.keyCode == 13)
                                        alert("Enter has been pressed!")
                            }} />
                    </div>
        }
    }

    _customIntro(){
        if(!this.state.isEditing){
            return <p>{ this.state.account.intro }</p>
        } else {
            return <textarea form="form">{ this.state.account.intro }</textarea>
        }
    }

    _customAboutMe(){
        if(!this.state.isEditing){
            return <p> { this.state.account.aboutMe }</p>
        } else {
            return <textarea form="form">{ this.state.account.aboutMe }</textarea>
        }
    }

    _update(){

    }

    _customUpdate(){
        if(this.state.isEditing){
            return  <div className="updateAccountBtnWrapper">
                        <a className="updateAccountBtn" onClick={()=> { alert('Update account!')}}>Update</a>
                    </div>
        }
    }

    _content(){
        if(this.state.account){
            return(
                <div id="account">
                    <form id="form">
                        <div className="switch-wrapper">
                            <label className="switch">
                                <input type="checkbox" value={ this.state.isEditing} onChange={(e) => { this._changeEditState(e.target.checked)}} />
                                <span className="slider"></span>
                            </label>
                            <p className="switch-description">Edit</p>
                        </div>
                        <div className="field">
                            { this._customAuthor() }
                        </div>
                        <div className="field">
                            { this._customEname() }
                        </div>
                        <div className="field">    
                            <label>Intro</label>
                            { this._customIntro() }
                        </div>
                        
                        <div className="field">    
                            <label>About me</label>
                            { this._customAboutMe() }
                        </div>
                        
                        <div className="field">
                            <label>Social Media</label>
                            <div className="social-media-wrapper">    
                                <div className="social-media">
                                    <a  target="_blank"
                                        href="http://www.facebook.com">
                                        <ion-icon name="logo-facebook"></ion-icon>
                                    </a>
                                </div>
                                <div className="social-media">
                                    <a  target="_blank"
                                        href="https://www.twitter.com">
                                        <ion-icon name="logo-twitter"></ion-icon>
                                    </a>
                                </div>
                                <div className="social-media">
                                    <a  target="_blank"
                                        href="https://www.instagram.com">
                                        <ion-icon name="logo-instagram"></ion-icon>
                                    </a>
                                </div>
                            </div>
                        </div>
                        { this._customUpdate() }
                    </form>
                </div>
            )
        }  else {
            return(
                <div className="loader-container">
                    <Loader 
                        type="Puff"
                        color="#2a2a2a"
                        height="50"	
                        width="50"
                    />
                </div>
            );
        }
    }
    render(){
        return this._content()
    }
}

const mapStateToProps = (state) => ({
    Session: state.Session
})

export default connect(mapStateToProps)(Index);