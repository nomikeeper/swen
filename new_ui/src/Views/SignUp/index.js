import React, { Component } from "react";
import { newsDB, userDB, firebase } from "../../Firebase";
require("../../style/css/signUp.css");

const types = {
    email: 'email',
    password: 'password',
    confirmPassword: 'confirmPassword'
}
class Index extends Component {
    constructor(props){
        super(props);
        this.state={
            email: '',
            password: '',
            confirmPassword: '',
            message: "",
            isValid: true,
            isCreated: false,
        }
    }

    // Create Account
    _createAccount(){
        if(this._validate()){
            if(this.state.password == this.state.confirmPassword){
                this.setState({
                    isValid: true,
                    message: '',
                })
                this._clearValues();

                firebase.auth().createUserWithEmailAndPassword(
                    this.state.email,
                    this.state.password
                ).then(resp => {
                    console.log("Result from firebase: ", resp)
                    let result = userDB.where("uid", "==", resp.user.uid)
                    userDB.add({
                        uid: resp.user.uid,
                        intro: '',
                        description: '',
                        socialMedia: []
                    })
                })
                .catch(error => {
                    console.log("Error: ", error)
                    this.setState({
                        isValid: false,
                        message: error.message
                    })
                })
                alert("Created Account");
            } else {
                console.log("Passwords doesn't match")
                this.setState({
                    password: '',
                    confirmPassword: '',
                    isValid: false,
                    message: "Passwords doesn't match"
                })
            }
        } else {
            this.setState({
                isValid: false,
                message: "E-mail is not valid"
            })
            this._clearValues();
        }
    }

    _validate(){
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(this.state.email).toLowerCase());
    }

    _updateValue(type, value){
        switch(type) {
            case types.email:{
                this.setState({ email: value });
                break;
            }
            case types.password: {
                this.setState({ password: value })
                break;
            }
            case types.confirmPassword: {
                this.setState({ confirmPassword: value })
                break;
            }
            default:{
                console.log("Invalid income!");
                break;
            }
        }
    }

    _clearValues(){
        this.setState({
            email: '',
            password: '',
            confirmPassword: ''
        })
    }


    render(){
        return(
            <div id="sign-up-wrapper">
                <form id="sign-up-form">
                    <div className="form-field" >
                        <label>E-mail</label>
                        <input  type="text" 
                                className="input"
                                placeholder="E-mail"
                                value={this.state.email} 
                                onChange={(e) => { this._updateValue(types.email, e.target.value ) }}        
                        />
                    </div>
                    <div className="form-field" >
                        <label>Password</label>
                        <input  type="password" 
                                className="input"
                                placeholder="Password"
                                value={this.state.password} 
                                onChange={(e) => { this._updateValue(types.password, e.target.value ) }}        
                        />
                    </div>
                    <div className="form-field" >
                        <label>Confirm password</label>
                        <input  type="password" 
                                className="input"
                                placeholder="Confirm password"
                                value={this.state.confirmPassword} 
                                onChange={(e) => { this._updateValue(types.confirmPassword, e.target.value ) }}        
                        />
                    </div>
                    {
                        
                        this.state.isValid ?
                            null
                            :
                            <div className="warning-wrapper">
                                <h3 className="warning-message">{ this.state.message }</h3>
                            </div>
                    }
                    <div className="create-account-btn-wrapper">
                        <a  className="create-account-btn"
                            onClick={()=>{ this._createAccount() }}>
                            Create Account
                        </a>
                    </div>
                </form>
            </div>
        )
    }
}

export default Index;