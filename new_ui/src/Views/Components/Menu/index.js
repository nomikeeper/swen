import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import { firebase } from "../../../Firebase";
import { sessionService } from "redux-react-session";
import SessionActions from "../../../Service/Actions/Session";
import Routes from "../../../Routes"
require("../../../style/css/login.css");

class Index extends Component {
    constructor(props){
        super(props);
        this.state={
            user: false,
            email: '',
            password: '',
            toMain: false,
            selectedView: '',
        }
    }

    componentWillMount(){
        console.log('Session Service: ', sessionService );
        sessionService.loadUser()
            .then(()=> { 
                this.setState({ user: true })
                console.log("This.props: ", this.props)
            })
            .catch(() => { this.setState({ user: false })})
        sessionService.loadSession()
            .then(currentSession => console.log(currentSession))
            .catch(err => console.log(err))
    }
    componentWillUnmount(){
        this.setState({ toMain: false})
    }

    // Update values
    _updateValue(type, value){
        if(type == "email"){
            this.setState({ email: value })
        } else {
            this.setState({ password: value })
        }
    }

    _navigate(path){
        if(this.props.changeSelectedView){
            this.props.changeSelectedView(path)
        } else {
            alert("Here!");
            this.setState({ selectedView: path, toMain: true })
        }
    }
    // Clear values
    _clearValues(){
        this.setState({ 
            email: '',
            password: ''
        })
    }
    // Login function
    _login(){
        firebase.auth().signInWithEmailAndPassword(
            this.state.email,
            this.state.password
        ).then(resp => { 
            sessionService.saveUser(resp.user)
            sessionService.saveSession()
                .then(() => { 
                    this.setState({ user: true })
                })
                .catch(()=>{ console.error("Failure")})
            this._clearValues();
        }).catch(error => {
            alert(error.message)
        })
    }

    _logOut(){
        sessionService.deleteUser()
            .then(() => { console.log("User has been removed from session" )})
            .catch(() => { console.log("Failed to remove the suer from the session." )})
        sessionService.deleteSession()
            .then(() => { 
                this.setState({ user: false })
                this.props.clearSelectedView()
             })
            .catch((error)=>{ console.error("Failure!", error)})
    }

    // Returns active if current button is selected
    _isActive(value){
         
        if(this.props.currenView == value)
            return 'active' 
        else
            return null 
    }
    // Returns the menu or login section depending on the session state
    _content(){
        if(this.state.user){
            return(
                <nav id="menu-list-wrapper">
                    
                    <ul id="menu-list">
                        <li className={`menu-item ${ this._isActive( MenuOptions.Dashboard )}`}><a onClick={()=>{ this._navigate(MenuOptions.Dashboard) }}>Dashboard</a></li>
                        <li className={`menu-item ${ this._isActive( MenuOptions.CreateTemplate )}`}><a onClick={()=>{ this._navigate(MenuOptions.CreateTemplate) }}>Create Template</a></li>
                        <li className="menu-item"><a href={ Routes.Draft }>Create Draft</a></li>
                        <li className={`menu-item ${ this._isActive( MenuOptions.TemplatesArchive )}`}><a onClick={()=>{ this._navigate(MenuOptions.TemplatesArchive) }}>Templates archive</a></li>
                        <li className={`menu-item ${ this._isActive( MenuOptions.TemplatesDrafts )}`}><a onClick={()=>{ this._navigate(MenuOptions.TemplatesDrafts) }}>Templates drafts</a></li>
                        <li className={`menu-item ${ this._isActive( MenuOptions.NewsDraft )}`}><a onClick={()=>{ this._navigate(MenuOptions.NewsDraft) }}>News drafts</a></li>
                        <li className={`menu-item ${ this._isActive( MenuOptions.News )}`}><a onClick={()=>{ this._navigate(MenuOptions.Account) }}>Account</a></li>
                        <li className="menu-item"><a onClick={()=>{ this._logOut() }}>Log out</a></li>
                    </ul>
                </nav> 
            )  
        } else {
            return(
                <div id="login">
                    <form id="form">
                        {/*
                        <h2 className="form-label">Welcome to Swen</h2>
                        */}
                        <div className="form-input-wrapper">
                            <input 
                                className="input"
                                type="text" 
                                name="Email"  
                                value={this.state.email}
                                placeholder={"E-mail"}
                                onChange={(e)=>{ this._updateValue('email', e.target.value) }}
                                />
                        </div>
                        <div className="form-input-wrapper">
                            <input 
                                className="input"
                                type="password" 
                                name="password"  
                                value={this.state.password}
                                placeholder={"Password"}
                                onChange={(e)=>{ this._updateValue('password', e.target.value) }}
                                />
                        </div>
                        <div className="login-btn-wrapper">
                            <a  className="login-btn"
                                onClick={()=>{ this._login() }}>
                                Login
                            </a>
                            <a  className="forgot-password"
                                onClick={()=>{ alert("Forgot password?")}}>
                                Forgot password?
                            </a>
                        </div>
                        <div className="login-btn-wrapper margin-top-25">
                            <a  className="sign-up-btn"
                                onClick={()=>{ this.props.changeSelectedView(MenuOptions.SignUp)}}>
                                Sign up
                            </a>
                        </div>
                    </form>
                </div>
            )
        }
    }
    // Main render function
    render(){
        return(
            <div id="menu">
                { this._content() }
            </div>
        );
    }
}

export default Index;

export const MenuOptions = {
    CreateTemplate: 'CreateTemplate',
    TemplatesArchive: 'TemplatesArchive',
    TemplatesDrafts: 'TemplatesDrafts',
    NewsDraft: "NewsDraft",
    News: "News",
    Account: 'Account',
    SignUp: "SignUp",
    Dashboard: "Dashboard",
    Intro: "Intro",
    Logout: 'Logout'
}