import React, { Component } from 'react';
import ReactPaginate from "react-paginate";
import Loader from 'react-loader-spinner';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCoffee } from '@fortawesome/free-solid-svg-icons';
import moment from 'moment';
// ------ Importing firebase collection
import { newsDB } from "../../Firebase";
require('../../style/css/library.css');

class Index extends Component {
    constructor(props){
        super(props);
        this.state={
            search: '',
            pageCount: 10,
            newsCol: null,
            news:null,
            isFetched: false,
            firstNews: null,
            lastNews: null,
        }
    }

    componentWillMount(){
        if(!this.state.isFetched){
            newsDB.get().then(docs => {
                this.setState({ 
                    pageCount: docs.docs.length%10 == 0 ? 
                        Math.floor(docs.docs.length/10) 
                        : 
                        Math.floor(docs.docs.length/10) + 1 
                },
                () => {
                    newsDB.orderBy('date', 'desc').limit(10).get().then(docs => {
                        const news = [];
                        docs.forEach( item => {
                            news.push(item)
                        })
                        this.setState({
                            newsCol: docs, 
                            news: news,
                            firstNews: docs.docs[0],
                            lastNews: docs.docs[docs.docs.length -1],
                            isFetched: true,
                        })
                    })
                })
            })
        }
    }

    // Next page
    nextPage(){
        newsDB.orderBy('date', 'desc').startAfter(this.state.lastNews).limit(10);
    }
    // Prev page
    prevPage(){
        newsDB.orderBy('date', 'desc').endBefore(this.state.firstNews).limit(10);
    }

    _applySearch(){
        console.log("Search parameter: ", this.state.search);
    }

    _updateSearchValue(value){
        this.setState({ search: value})
    }

    // Handle click
    _handlePageClick(){

    }

    _content(){
        if(this.state.news){
            return(
                <div id="list-wrapper">
                    <h3>{this.props.type}</h3>
                    <div className="search-wrapper">
                        <input 
                            className="search-field"
                            type="text" 
                            placeholder="Search"
                            onChange={(e) => { this._updateSearchValue(e.target.value) }}    
                            onKeyPress={(e)=>{
                                if(e.key == "enter"){
                                    this._applySearch();
                                }
                            }}
                        />
                        <a onClick={()=>{alert("Apply search")}} className="search-btn-wrapper">
                            <ion-icon name="ios-search"></ion-icon>
                        </a>
                    </div>
                    <nav id="list">
                        <div className="row">
                            <div className="index-column"> </div>
                            <div className="title-column">Title</div>
                            <div className="desc-column">Description</div>
                            <div className="date-column">Date</div>
                            <div className="view-column">
                                <a className="view-btn">Function</a>
                            </div>
                        </div>
                        {
                            this.state.news ? 
                                this.state.news.map((item, index) => {
                                    console.log("Item: ", item.data())
                                    return(
                                        <div className="row" key={ (this.state.pageCount - 1) * 10 + index }>
                                            <div className="index-column">{ (this.state.pageCount - 1) * 10 + index + 1 }</div>
                                            <div className="title-column">{ item.data().title }</div>
                                            <div className="desc-column">{ item.data().context }</div>
                                            <div className="date-column">{ moment(item.data().date).format('YYYY-MM-DD') }</div>
                                            <div className="view-column">
                                                <a  className="view-btn" 
                                                    onClick={()=>{ this.props.setNewsID(item.data().id)}} 
                                                    //href={`/News/${item.data().id}`}
                                                    >
                                                    View
                                                </a>
                                            </div>
                                        </div>
                                    )
                                })
                                :
                                <h3 className="loading">Loading</h3>
                        }
                    </nav>
                    <div className="pagination-wrapper">
                        <ReactPaginate 
                            previousLabel={<ion-icon name="ios-arrow-back"></ion-icon>}
                            nextLabel={<ion-icon name="ios-arrow-forward"></ion-icon>}
                            breakLabel={<a href="">...</a>}
                            breakClassName={"break-me"}
                            pageCount={this.state.pageCount}
                            marginPagesDisplayed={2}
                            pageRangeDisplayed={5}
                            onPageChange={this._handlePageClick.bind(this)}
                            containerClassName={"pagination"}
                            pageClassName={"pages"}
                            pageLinkClassName={"pages-btn"}
                            activeClassName={"active"}
                            previousClassName={"prev"}
                            nextClassName={"next right-border"}
                            disabledClassName={"disabled"}
                            />
                    </div>
                </div>
            );
        } else {
            return(
                <div className="loader-container">
                    <Loader 
                        type="Puff"
                        color="#2a2a2a"
                        height="50"	
                        width="50"
                    />
                </div>
            );
        }
    }

    render(){
        return(
            <div id="wrapper">
                { // Content selector
                    this._content()
                }
            </div>
        )
    }
}

export default Index;

const list = [
    {
        title: 'title - 1',
        description: 'description - 1',
        date: '2018-06-24'
    },
    {
        title: 'title - 2',
        description: 'description - 2',
        date: '2018-06-24'
    },
    {
        title: 'title - 3',
        description: 'description - 3',
        date: '2018-06-24'
    },
    {
        title: 'title - 4',
        description: 'description - 4',
        date: '2018-06-24'
    },
    {
        title: 'title - 5',
        description: 'description - 5',
        date: '2018-06-24'
    },
    {
        title: 'title - 6',
        description: 'description - 6',
        date: '2018-06-24'
    },
    {
        title: 'title - 7',
        description: 'description - 7',
        date: '2018-06-24'
    },
]