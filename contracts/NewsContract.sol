pragma solidity ^0.4.23;

contract NewsContract{

    uint newsCounter = 0;

    struct News{
        string title;
        string context;
        string date;
    }

    mapping (uint => News) public NewsArchive;
    address[] public newsAddresses;

    constructor() public {
    }

    function createNews( string _title, string _context, string _date) public returns( uint ) {
        
        var news = NewsArchive[newsCounter];

        news.title = _title;
        news.context = _context;
        news.date = _date;

        newsCounter++;

        return( newsCounter - 1 );
    }

    function getNews(uint _index) public view returns(
        string _title,
        string _context,
        string _date
    ){
        var news = NewsArchive[_index];
        return(news.title, news.context, news.date);
    }

    function getAllNews() view public returns(address[]){
        return newsAddresses;
    }

    function totalNewsCount() view public returns( uint ){
        return newsCounter;
    }
}
